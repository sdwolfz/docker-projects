#!/usr/bin/env bash

set -e

echo '# -----------------------------------------------------------------------'
echo 'Configure reverse proxy'
echo ''
set -x

if docker container ls -a | grep -q traefik-reverse-proxy; then
  docker start traefik-reverse-proxy
else
  docker run -d                                                \
    -l 'traefik.enabled=true'                                  \
    -l 'traefik.http.routes.traefik.entrypoints=traefik'       \
    -l 'traefik.http.routes.traefik.rule=Host(`traefik.test`)' \
    --name=traefik-reverse-proxy                               \
    --network=docker-compose-shared-network                    \
    -p 80:80 -p 12345:8080                                     \
    --restart=on-failure                                       \
    -v /var/run/docker.sock:/var/run/docker.sock               \
    traefik:latest                                             \
      --accesslog=true                                         \
      --api.insecure=true                                      \
      --entrypoints.http.address=:80                           \
      --entrypoints.traefik.address=:8080                      \
      --providers.docker                                       \
      --providers.docker.exposedbydefault=false                \
      --providers.docker.network=docker-compose-shared-network
fi

set +x
echo ''
