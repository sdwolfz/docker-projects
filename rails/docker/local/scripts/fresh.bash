#!/usr/bin/env bash

set -e

echo '# -----------------------------------------------------------------------'
echo 'Fresh install'
echo ''
set -x

# Setting up user UID and GID for later use
export HOST_USER_UID=`id -u`
export HOST_USER_UID=`id -g`

# Build docker images
docker-compose build --pull

docker-compose up -d postgres

docker-compose run --rm -T web bundle install
docker-compose run --rm -T web bundle exec rake db:drop db:create db:migrate db:seed

docker-compose up -d

set +x
echo ''
