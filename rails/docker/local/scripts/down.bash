#!/usr/bin/env bash

set -e

echo '# -----------------------------------------------------------------------'
echo 'Destroy containers and volumes'
echo ''
set -x

docker-compose down -v --remove-orphans

set +x
echo ''
echo '# -----------------------------------------------------------------------'
echo 'If you want to recreate the reverse proxy, execute'
echo ''
echo 'make proxy'
echo ''
set -x
