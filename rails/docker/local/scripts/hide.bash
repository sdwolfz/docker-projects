#!/usr/bin/env bash

set -e

# NOTE: you must not remove `traefik.test` as it might be used somewhere else!

echo '# -----------------------------------------------------------------------'
echo 'Hide local domain names'
echo ''
set -x

hide_entry() {
  entry=$1
  if grep -q "$entry" /etc/hosts; then
    grep -v "^$entry\$" /etc/hosts | sudo tee /etc/hosts > /dev/null
    echo "$entry removed from /etc/hosts"
  else
    echo "$entry does not exist in /etc/hosts"
  fi
}

hide_entry '127.0.0.1 whatever-app.test'

set +x
echo ''
