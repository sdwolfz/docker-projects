#!/usr/bin/env bash

set -e

echo '# -----------------------------------------------------------------------'
echo 'Create env files'
echo ''
set -x

# Create `env` directory in your project root
mkdir -p ./env/

# Ensure it's not removed by `git clean -fdx`
git init -q ./env/

# Copy template env files
cp -f ./docker/local/config/postgres.env.template ./env/postgres.env
cp -f ./docker/local/config/web.env.template ./env/web.env

set +x
echo ''
