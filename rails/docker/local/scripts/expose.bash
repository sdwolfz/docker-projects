#!/usr/bin/env bash

set -e

echo '# -----------------------------------------------------------------------'
echo 'Expose local domain names'
echo ''
set -x

expose_entry() {
  entry=$1
  if grep -q "$entry" /etc/hosts; then
    echo "$entry already exists in /etc/hosts"
  else
    echo "$entry" | sudo tee -a /etc/hosts
    echo "$entry added to /etc/hosts"
  fi
}

expose_entry '127.0.0.1 traefik.test'
expose_entry '127.0.0.1 whatever-app.test'

set +x
echo ''
