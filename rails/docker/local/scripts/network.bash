#!/usr/bin/env bash

set -e

echo '# -----------------------------------------------------------------------'
echo 'Configure docker shared network'
echo ''
set -x

docker network ls | grep -q docker-compose-shared-network \
  || docker network create --driver bridge docker-compose-shared-netowrk

set +x
echo ''
