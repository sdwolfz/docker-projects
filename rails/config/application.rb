require_relative "boot"

require "rails/all"

Bundler.require(*Rails.groups)

module WhateverApp
  class Application < Rails::Application
    config.load_defaults 7.0

    hosts = ENV['RAILS_HOSTS']
    if hosts
      hosts.split(',').each do |host|
        config.hosts << host
      end
    end
  end
end
