# Docker Projects

Docker setup templates for diverse projects.

## License

The rest of the code is covered by the BSD 3-clause License, see
[LICENSE](LICENSE) for more details.
